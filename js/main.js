

$(document).ready(function () {


	// $(".icon-bar").click(function () {
	// 	$("header#header").toggleClass("active-menu");
	// });
	$(".toggle-search").click(function () {
		$(".wrap-search-header").toggleClass("active");
	});
	$(document).mouseup(function (e) {
		var container = $(".wrap-search-header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.wrap-search-header').removeClass('active');
		}
	});

	// menu
	$(".icon-bar").click(function () {
		$('.menu .icon-bar').toggleClass('active');
		$('body').toggleClass('overlay');
		$('nav#nav').toggleClass('active');
		$('header#header').toggleClass('active-menu');
		$('main#main').toggleClass('active-menu');
		$('footer#footer').toggleClass('active-menu');
	});
	$(document).mouseup(function (e) {
		var container = $(".menu");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$('.menu .icon-bar').removeClass('active');
				$('body').removeClass('overlay');
				$('nav#nav').removeClass('active');
				$('header#header').removeClass('active-menu');
				$('main#main').removeClass('active-menu');
				$('footer#footer').removeClass('active-menu');
		}
	});



	function setWidthText() {
		let showChar = 350;
		var ellipsestext = "...";
		var data = ['box-content-ykien p'];
		data.forEach(function (value) {

			$('.' + value).each(function () {
				var content = $(this).html();
				if (content.length > showChar) {
					var c = content.substr(0, showChar);
					var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp';
					$(this).html(html);
				}

			});
		});
	}
	setWidthText();

	$(".toggle-form-search").click(function () {
		$(this).toggleClass('active');
		$(this).siblings('form').toggleClass('active');
	});
	$(document).mouseup(function (e) {
		var container = $(".search-main");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.search-main .toggle-form-search').removeClass('active');
			$('.search-main form').removeClass('active');
		}
	});


	// menu footer 

	$(".icon-toggleSubmenu").click(function () {
		$(this).children().attr('src', function (index, attr) {
			return attr == './images/icon-plus-submenu.png' ? './images/icon-minus-submenu.png' : './images/icon-plus-submenu.png';
		});
		$(this).siblings('.wrapper_naviga').slideToggle();
	});

	// scroll top 
	$('.scroll-top').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 500)
	});
	var offset = 600,
		$back_to_top = $('.scroll-top');
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('visible-top') : $back_to_top.removeClass('visible-top');
	});

	// cover size text 

	var count = 16;
    $("#minus").click(function(){
        if (count > 10) {
        count--;
		$('.wrap-blog-detail-main *').css('font-size', +count+'px');
    }
    });
    $("#plus").click(function(){
        if (count <25) {
          count++;
		   $('.wrap-blog-detail-main *').css('font-size', +count+'px');
        }
    });
	$('.cover-size span:first-child').click(function(){
		$('.wrap-blog-detail-main *').css('font-size', '16px');
	});

	if($('.swiper').hasClass('wrap-item-scroll')){
		var swiper = new Swiper(".wrap-item-scroll", {
			slidesPerView: "auto",
			spaceBetween: 0,
		});
	}


	if($('.swiper').hasClass('wrap-item-scroll')){
		$('.wrap-item-scroll').append(`<div class="wrap-icon-control"></div>`);
	}

});




$('.tab-link').click(function () {

	var tabID = $(this).attr('data-tab');

	$(this).addClass('active').siblings().removeClass('active');

	$('#tab-' + tabID).addClass('active').siblings().removeClass('active');
});

$('#slide-index').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	responsive: {
		0: {
			items: 1,
			loop: ($('#slide-index .item').length > 1) ? true : false,
		}
	}
});
$('#slide-hot-blog-list').owlCarousel({
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	responsive: {
		0: {
			items: 1,
			// loop: ($('#slide-hot-blog-list .item').length > 1) ? true : false,
		}
	}
});
$('#slide-customer').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 15,
	responsive: {
		0: {
			items: 2,
			loop: ($('#slide-customer .item').length > 2) ? true : false,
		},
		575: {
			items: 3,
			loop: ($('#slide-customer .item').length > 3) ? true : false,
		},
		768: {
			items: 4,
			loop: ($('#slide-customer .item').length > 4) ? true : false,
		},
		991: {
			items: 4,
			loop: ($('#slide-customer .item').length > 4) ? true : false,
		},
		1024: {
			items: 6,
			loop: ($('#slide-customer .item').length > 6) ? true : false,
		}
	}
});

// fix header 
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-140px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}

$(window).on('scroll', function () {
		if ($(window).scrollTop()) {
			$('#header:not(.header-project-detail-not-scroll)').addClass('active');
		} else {
			$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
		};
	});


	// search header
	$(".toggle-search-header").click(function () {
		$(this).toggleClass('active');
		$(".wrap-search-header").toggleClass('active');
		$(".wrap-search-header").siblings('form').toggleClass('active');
	});
	$(document).mouseup(function (e) {
		var container = $(".wrap-search-header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.wrap-search-header').removeClass('active');
			$(".toggle-search-header").removeClass('active');
		}
	});

	function resizeImage(){
	let arrClass = [
		{class:'corver-size-banner-mamin', number:(625/1900)},
		{class:'cover-size-about', number:(241/362)},
		{class:'cover-size-service', number:(241/365)},
		{class:'cover-size-2', number:(199/300)},
		{class:'cover-size-3', number:(199/300)},
		{class:'cover-size-staff', number:(332/284)},
		{class:'cover-size-blog', number:(203/362)},
		{class:'cover-size-blog-hot', number:(191/339)},
		{class:'cover-size-image-libary-zoom', number:(257/384)},
	];
	for (let i = 0; i < arrClass.length; i++) {
			let width = $("."+arrClass[i]['class']).width();
			$("."+arrClass[i]['class']).css('height', width*arrClass[i]['number']+'px');
			console.log(width);
			console.log(arrClass);
			console.log(width*arrClass[i]['number']);
		}
	}
	resizeImage();
$(window).on('resize', function(){
	resizeImage();
});

// active youtube
function activeUrlVideo(attr){
	var id_video = $(attr).data('url');
	var symbol = $("#video-main")[0].src;
	$("#video-main")[0].src = 'https://www.youtube.com/embed/' + id_video + '?autoplay=1';
}
function closeVideo() {
	var symbol = $("#video-main")[0].src;
	var arr = symbol.split('embed/');
	$("#video-main")[0].src = arr[0] + 'embed/';
}
$(".active-video").click(function () {
	activeUrlVideo(this);
});
$(".close").click(function () {
	closeVideo();
});
$(document).mouseup(function (e) {
	var container = $("#video-youtube .wrap-video-youtube");
	if (!container.is(e.target) &&
		container.has(e.target).length === 0) {
		if ($('.modal').hasClass('show')) {
			closeVideo();
		}
	}
});

// count  number
if($('#count-number').hasClass('wrap-number-about')){
	$('#about-index').waypoint(function () {
		$(".counter").each(function () {
			var $this = $(this),
				countTo = $this.attr("data-countto");
			countDuration = parseInt($this.attr("data-duration"));
			$({ counter: $this.text() }).animate(
				{
					counter: countTo
				},
				{
					duration: countDuration,
					easing: "linear",
					step: function () {
						$this.text(Math.floor(this.counter));
					},
					complete: function () {
						$this.text(this.counter);
					}
				}
			);
		});
	}, {
		offset: '100%'
	});
};
